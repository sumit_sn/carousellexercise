/**
 * 
 */
package com.carousell.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author snarayan
 *
 */
public class Channel {
	
	/** The id. */
	private String id;
	
	/** The name. */
	private String name;
	
	public Channel() {
		
	}
	
	public Channel(String id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	@JsonProperty(value = "id")
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@JsonProperty(value = "name")
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
}
