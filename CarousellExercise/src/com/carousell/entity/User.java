/**
 * 
 */
package com.carousell.entity;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class User.
 *
 * @author snarayan
 */
public class User implements Comparable<User> {
	
	/** The id. */
	private String id;
	
	/** The name. */
	private String name;
	
	/** The rating. */
	private String rating;
	
	/** The published topics. */
	private List<Topic> publishedTopics = new ArrayList<>();
	
	/** The upvoted topics. */
	private List<Topic> upvotedTopics = new ArrayList<>();
	
	/** The downvoted topics. */
	private List<Topic> downvotedTopics = new ArrayList<>();
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	@JsonProperty(value = "id")
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@JsonProperty(value = "name")
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the rating.
	 *
	 * @return the rating
	 */
	@JsonProperty(value = "rating")
	public String getRating() {
		return rating;
	}

	/**
	 * Sets the rating.
	 *
	 * @param rating the new rating
	 */
	public void setRating(String rating) {
		this.rating = rating;
	}

	/**
	 * Gets the published topics.
	 *
	 * @return the published topics
	 */
	@JsonProperty(value = "publishedTopics")
	public List<Topic> getPublishedTopics() {
		return publishedTopics;
	}

	/**
	 * Sets the published topics.
	 *
	 * @param publishedTopics the new published topics
	 */
	public void setPublishedTopics(List<Topic> publishedTopics) {
		this.publishedTopics = publishedTopics;
	}

	/**
	 * Gets the upvoted topics.
	 *
	 * @return the upvoted topics
	 */
	@JsonProperty(value = "upvotedTopics")
	public List<Topic> getUpvotedTopics() {
		return upvotedTopics;
	}

	/**
	 * Sets the upvoted topics.
	 *
	 * @param upvotedTopics the new upvoted topics
	 */
	public void setUpvotedTopics(List<Topic> upvotedTopics) {
		this.upvotedTopics = upvotedTopics;
	}

	/**
	 * Gets the downvoted topics.
	 *
	 * @return the downvoted topics
	 */
	@JsonProperty(value = "downvotedTopics")
	public List<Topic> getDownvotedTopics() {
		return downvotedTopics;
	}

	/**
	 * Sets the downvoted topics.
	 *
	 * @param downvotedTopics the new downvoted topics
	 */
	public void setDownvotedTopics(List<Topic> downvotedTopics) {
		this.downvotedTopics = downvotedTopics;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(User o) {
		return id.compareTo(o.getId());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		return (o != null)?id.equals(((User)o).getId()):false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return id.hashCode();
	}
}
