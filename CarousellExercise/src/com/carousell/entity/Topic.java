/**
 * 
 */
package com.carousell.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * The Class Topic.
 *
 * @author snarayan
 */
@JsonTypeName(value="topic")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Topic.class)
public class Topic implements Comparable<Topic> {
	
	/** The id. */
	private String id;
	
	/** The content. */
	private String content;
	
	/** The upvotes. */
	private int upvotes;
	
	/** The downvotes. */
	private int downvotes;
	
	/** The published by user. */
	private User publishedByUser;
	
	/** The channel. */
	private Channel channel;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	@JsonProperty(value = "id")
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the content.
	 *
	 * @return the content
	 */
	@JsonProperty(value = "content")
	public String getContent() {
		return content;
	}

	/**
	 * Sets the content.
	 *
	 * @param content the new content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Gets the upvotes.
	 *
	 * @return the upvotes
	 */
	@JsonProperty(value = "upvotes")
	public int getUpvotes() {
		return upvotes;
	}

	/**
	 * Sets the upvotes.
	 *
	 * @param upvotes the new upvotes
	 */
	public void setUpvotes(int upvotes) {
		this.upvotes = upvotes;
	}

	/**
	 * Gets the downvotes.
	 *
	 * @return the downvotes
	 */
	@JsonProperty(value = "downvotes")
	public int getDownvotes() {
		return downvotes;
	}

	/**
	 * Sets the downvotes.
	 *
	 * @param downvotes the new downvotes
	 */
	public void setDownvotes(int downvotes) {
		this.downvotes = downvotes;
	}

	/**
	 * Gets the published by user.
	 *
	 * @return the published by user
	 */
	@JsonProperty(value = "publishedByUser")
	public User getPublishedByUser() {
		return publishedByUser;
	}

	/**
	 * Sets the published by user.
	 *
	 * @param publishedByUser the new published by user
	 */
	public void setPublishedByUser(User publishedByUser) {
		this.publishedByUser = publishedByUser;
	}

	/**
	 * Gets the channel.
	 *
	 * @return the channel
	 */
	@JsonProperty(value = "channel")
	public Channel getChannel() {
		return channel;
	}

	/**
	 * Sets the channel.
	 *
	 * @param channel the new channel
	 */
	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Topic o) {
		return id.compareTo(o.getId());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		return (o != null)?id.equals(((Topic)o).getId()):false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return id.hashCode();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "id: " + id + " content: " + content + " upvotes: " + upvotes
				+ " downvotes: " + downvotes + " publishedByUser: "
				+ publishedByUser + " channel: " + channel + "\n";

	}
}
