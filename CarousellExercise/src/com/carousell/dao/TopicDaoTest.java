/**
 * 
 */
package com.carousell.dao;

import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.carousell.entity.Channel;
import com.carousell.entity.Topic;

/**
 * @author snarayan
 *
 */
public class TopicDaoTest {

	private TopicDao topicDao = null;
	
	private Topic []topics = {new Topic(), new Topic(), new Topic(), new Topic()};
	
	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger("TopicDaoTest.class");
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		topicDao = TopicDao.getInstance();
		topics[0].setId("TOPIC0001");
		topics[0].setChannel(new Channel("CHN001","Politics"));
		topics[0].setContent("My first topic");
		topics[0].setDownvotes(1);
		topics[0].setUpvotes(5);
		
		topics[1].setId("TOPIC0002");
		topics[1].setChannel(new Channel("CHN001","Politics"));
		topics[1].setContent("My second topic");
		topics[1].setDownvotes(2);
		topics[1].setUpvotes(4);
		
		topics[2].setId("TOPIC0003");
		topics[2].setChannel(new Channel("CHN002","Lifestyle"));
		topics[2].setContent("My third topic");
		topics[2].setDownvotes(2);
		topics[2].setUpvotes(6);
		
		topics[3].setId("TOPIC0004");
		topics[3].setChannel(new Channel("CHN002","Lifestyle"));
		topics[3].setContent("My fourth topic");
		topics[3].setDownvotes(0);
		topics[3].setUpvotes(4);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.carousell.dao.TopicDao#newTopic(com.carousell.entity.Topic)}.
	 */
	@Test
	public void testNewTopic() {
		topicDao.newTopic(topics[0]);
		topicDao.newTopic(topics[1]);
		topicDao.newTopic(topics[2]);
		topicDao.newTopic(topics[3]);
		logger.info(topicDao.pollTopicQueueToString());
	}

	/**
	 * Test method for {@link com.carousell.dao.TopicDao#upvoteTopic(java.lang.String)}.
	 */
	@Test
	public void testUpvoteTopic() {
		topicDao.newTopic(topics[0]);
		topicDao.newTopic(topics[1]);
		topicDao.upvoteTopic("TOPIC0001");
		logger.info(topicDao.pollTopicQueueToString());
	}

	/**
	 * Test method for {@link com.carousell.dao.TopicDao#downvoteTopic(java.lang.String)}.
	 */
	@Test
	public void testDownvoteTopic() {
		topicDao.newTopic(topics[0]);
		topicDao.newTopic(topics[1]);
		topicDao.downvoteTopic("TOPIC0002");
		logger.info(topicDao.pollTopicQueueToString());
	}

	/**
	 * Test method for {@link com.carousell.dao.TopicDao#getAllTopicsForChannel(java.lang.String)}.
	 */
	@Test
	public void testGetAllTopicsForChannel() {
		topicDao.newTopic(topics[0]);
		topicDao.newTopic(topics[1]);

		topicDao.getAllTopicsForChannel("CHN001").forEach(
				t -> logger.info(t.toString()));
	}

	/**
	 * Test method for {@link com.carousell.dao.TopicDao#getTopNTopics()}.
	 */
	@Test
	public void testGetTopNTopics() {
		topicDao.newTopic(topics[0]);
		topicDao.newTopic(topics[1]);
		topicDao.newTopic(topics[2]);
		topicDao.newTopic(topics[3]);
		int n = (topicDao.getCurrentNoOfTopics() > 20) ? 20 : topicDao
				.getCurrentNoOfTopics();
		topicDao.getTopNTopics(n);
	}
}
