/**
 * 
 */
package com.carousell.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.logging.Logger;

import com.carousell.entity.Topic;

/**
 * The Class TopicDao.
 *
 * @author snarayan
 */
public class TopicDao {
	
	/** The topic dao. */
	private static TopicDao topicDao = new TopicDao();
	
	/** The topics. */
	private static PriorityQueue<Topic> topics = new PriorityQueue<>((Topic o1,
			Topic o2) -> (o1.getUpvotes() == o2.getUpvotes()) ? 0
			: o2.getUpvotes() - o1.getUpvotes()); 
	
	/** The upvoted. */
	private boolean upvoted = false;
	
	/** The downvoted. */
	private boolean downvoted = false;
	
	/** The returned topics. */
	private List<Topic> returnedTopics = new ArrayList<>();
	
	/** The temp. */
	private Topic temp;
	
	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger("TopicDao.class");
	
	/**
	 * Instantiates a new topic dao.
	 */
	private TopicDao() {
		
	}
	
	/**
	 * Gets the single instance of TopicDao.
	 *
	 * @return single instance of TopicDao
	 */
	public static TopicDao getInstance() {
		return topicDao;
	}
	
	/**
	 * New topic.
	 *
	 * @param topic the topic
	 * @return true, if successful
	 */
	public boolean newTopic(Topic topic) {
		logger.info("Input Topic: "+topic.toString());
		return topics.offer(topic);
	}
	
	/**
	 * Upvote topic.
	 *
	 * @param topicId the topic id
	 * @return true, if successful
	 */
	public boolean upvoteTopic(String topicId) {
		logger.info("Input TopicId: "+topicId);
		upvoted = false;
		topics.forEach(t -> {
			if (t.getId().equalsIgnoreCase(topicId)) {
				temp = t;
				upvoted = true;
			}
		});
		if(upvoted) {
			topics.remove(temp);
			temp.setUpvotes(temp.getUpvotes() + 1);
			topics.offer(temp);
		}
		return upvoted;
	}
	
	/**
	 * Delete topic.
	 *
	 * @param topicId the topic id
	 * @return true, if successful
	 */
	public boolean deleteTopic(String topicId) {
		logger.info("Input TopicId: "+topicId);
		temp = null;
		topics.forEach(t -> {
			if (t.getId().equalsIgnoreCase(topicId)) {
				temp = t;
			}
		});
		if(temp != null) {
			return topics.remove(temp);
		}
		else {
			return false;
		}
	}
	
	/**
	 * Downvote topic.
	 *
	 * @param topicId the topic id
	 * @return true, if successful
	 */
	public boolean downvoteTopic(String topicId) {
		logger.info("Input TopicId: "+topicId);
		downvoted = false;
		topics.forEach(t -> {
			if (t.getId().equalsIgnoreCase(topicId)) {
				temp = t;
				downvoted = true;
			}
		});
		if(downvoted) {
			topics.remove(temp);
			temp.setDownvotes(temp.getDownvotes() + 1);
			topics.offer(temp);
		}
		return downvoted;
	}
	
	/**
	 * Gets the all topics for channel.
	 *
	 * @param channelId the channel id
	 * @return the all topics for channel
	 */
	public List<Topic> getAllTopicsForChannel(String channelId) {
		logger.info("Input ChannelId: "+channelId);
		returnedTopics.clear();
		topics.forEach(t -> {
			if (t.getChannel().getId().equalsIgnoreCase(channelId)) {
				returnedTopics.add(t);
			}
		});
		return returnedTopics;
	}
	
	/**
	 * Gets the top n topics.
	 *
	 * @param n the n
	 * @return the top n topics
	 */
	public List<Topic> getTopNTopics(int n) {
		returnedTopics.clear();
		Topic val = null;
		while((val = topics.poll()) != null && n > 0) {
			returnedTopics.add(val);
			logger.info(val.toString());
			n--;
		}
		returnedTopics.forEach(t -> topics.offer(t));
		return returnedTopics;
	}
	
	/**
	 * Topic queue to string.
	 *
	 * @return the string
	 */
	public String pollTopicQueueToString() {
		StringBuffer buf = new StringBuffer();
		Topic val = null;
		while( (val = topics.poll()) != null) {
		    buf.append(val.toString());
		}
		return buf.toString();
	}
	
	/**
	 * Gets the current no of topics.
	 *
	 * @return the current no of topics
	 */
	public int getCurrentNoOfTopics() {
		return topics.size();
	}
}