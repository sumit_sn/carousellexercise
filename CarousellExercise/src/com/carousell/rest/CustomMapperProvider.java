/**
 * 
 */
package com.carousell.rest;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;

/**
 * @author snarayan
 *
 */
@Provider
public class CustomMapperProvider implements ContextResolver<ObjectMapper> {
    
    private static final ObjectMapper mapper = new ObjectMapper();
    
    static {
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		mapper.getSerializationConfig().withInsertedAnnotationIntrospector(
				new JacksonAnnotationIntrospector());
		mapper.getSerializationConfig().withSerializationInclusion(
				JsonInclude.Include.NON_NULL);
    }
    
    public static ObjectMapper getMapper() {
		return mapper;
    }
      
    @Override
    public ObjectMapper getContext(Class<?> type) {
        return mapper;
    } 
}