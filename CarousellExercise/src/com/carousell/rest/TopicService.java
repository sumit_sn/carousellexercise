package com.carousell.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.carousell.dao.TopicDao;
import com.carousell.entity.Topic;
import com.carousell.rest.ApiResponse.ResponseStatus;

import java.util.List;
import java.util.logging.Logger;

/**
 * The Class TopicService.
 *
 * @author snarayan
 * REST End Point for Exposing Topic Entity 
 */
@Path("/topic")
public class TopicService
{
	
	/** Logger Instance. */
	private static final Logger logger = Logger.getLogger("TopicService.class");

	/** The Constant TOPIC_ID. */
	private static final String TOPIC_ID = "id";
	
	/** The Constant CHANNEL_ID. */
	private static final String CHANNEL_ID = "id";

	/**
	 * Delete topic.
	 *
	 * @param topicID the topic ID
	 * @return the response
	 * @api {delete} /topic/delete/:id Delete Topic Entity
	 * @apiName DeleteTopic
	 * @apiGroup Topic
	 * @apiParam (Topic) {String} id Topic unique ID
	 * @apiSuccess {json} Success-Response: {
	 * 		 "status": "deleted"
	 *       "message": "Topic Entity Deleted"
	 *     }	
	 * @apiError {json} Error-Response: {
	 *       "status": "dberror"
	 *       "message": ""
	 *     }
	 */
	@DELETE
	@Path("/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteTopic(@PathParam(TOPIC_ID) String topicID)
	{
		// delete the Topic Entity
		logger.info("\n**Entry: deleteTopic** " + "Topic: " + topicID);
		if(TopicDao.getInstance().deleteTopic(topicID)) {
			return Response.status(200).entity(
					new ApiSuccess("", ResponseStatus.DELETED,"Topic Entity Deleted")).build();
		}
		else {
			return Response.status(200).entity(
					new ApiSuccess("", ResponseStatus.ERROR,"Error")).build();
		}
	}
	
	/**
	 * Upvote topic.
	 *
	 * @param topicID the topic ID
	 * @return the topic
	 * @api {get} /topic/upvote/:id Upvote Topic
	 * @apiName UpvoteTopic
	 * @apiGroup Topic
	 * @apiParam {String} id Topic unique ID.
	 * @apiSuccess {Boolean} success success.
	 * @apiError {json} Error-Response: {
	 *       "status": "no_entry_exists"
	 *       "message": ""
	 *     }  
	 * @apiError {json} Error-Response: {
	 *       "status": "error"
	 *       "message": ""
	 *     }    
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/upvote/{id}")
	public Response upvoteTopic(@PathParam(TOPIC_ID) String topicID) {
		// retrieve the Topic Entity identified by topicID
		logger.info("\n**Entry: upvoteTopic** " + "TopicID: " + topicID);
		if(TopicDao.getInstance().upvoteTopic(topicID)) {
			return Response.status(200).entity(
					new ApiSuccess("", ResponseStatus.SUCCESS,"Topic upvoted")).build();
		}
		else {
			return Response.status(200).entity(
					new ApiSuccess("", ResponseStatus.ERROR,"Error")).build();
		}
	}

	/**
	 * Downvote topic.
	 *
	 * @param topic the topic
	 * @return the response
	 * @api {get} /topic/downvote/:id Downvote Topic
	 * @apiName DownvoteTopic
	 * @apiGroup Topic
	 * @apiParam {String} id Topic unique ID.
	 * @apiSuccess {Boolean} success success.
	 * @apiError {json} Error-Response: {
	 *       "status": "no_entry_exists"
	 *       "message": ""
	 *     }  
	 * @apiError {json} Error-Response: {
	 *       "status": "error"
	 *       "message": ""
	 *     }	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/downvote/{id}")
	public Response downvoteTopic(@PathParam(TOPIC_ID) String topicID) {
		// retrieve the Topic Entity identified by topicID
		logger.info("\n**Entry: downvoteTopic** " + "TopicID: " + topicID);
		if(TopicDao.getInstance().downvoteTopic(topicID)) {
			return Response.status(200).entity(
					new ApiSuccess("", ResponseStatus.SUCCESS,"Topic downvoted")).build();
		}
		else {
			return Response.status(200).entity(
					new ApiSuccess("", ResponseStatus.ERROR,"Error")).build();
		}
	}

	/**
	 * Creates the topic.
	 *
	 * @param topic the topic
	 * @return the response
	 * @api {post} /topic/create Create Topic Entity
	 * @apiName CreateTopic
	 * @apiGroup Topic
	 * @apiParam (Topic) {Object} topic Topic Entity to be created
	 * @apiSuccess {Boolean} success success.
	 * @apiError {json} Error-Response: {
	 *       "status": "dberror"
	 *       "message": ""
	 *     }
	 */
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createTopic(Topic topic)
	{
		// create the Topic Entity and return the result
		logger.info("\n**Entry: createTopic** "+ "Id: " + topic.getId()+ " Name: " + topic.getContent());
		if(TopicDao.getInstance().newTopic(topic)) {
			return Response.status(200).entity(
					new ApiSuccess("", ResponseStatus.SUCCESS,"Topic created")).build();
		}
		else {
			return Response.status(200).entity(
					new ApiSuccess("", ResponseStatus.ERROR,"Error")).build();
		}
	}

	/**
	 * Gets the topics for a Channel.
	 *
	 * @param channelID the channel ID
	 * @return the list of topics
	 * @api {get} /topic/topicsByChannel/:id Fetch List of Topics
	 * @apiName GetTopics
	 * @apiGroup Topic
	 * @apiParam {String} id Channel ID.
	 * @apiSuccess {Object[]} topics Topics for the Channel.
	 * @apiError {json} Error-Response: {
	 *       "status": "no_entry_exists"
	 *       "message": ""
	 *     }  
	 * @apiError {json} Error-Response: {
	 *       "status": "error"
	 *       "message": ""
	 *     }
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/topicsByChannel/{id}")
	public Response getTopics(@PathParam(CHANNEL_ID) String channelID) {
		// retrieve the Topics identified by channelID
		logger.info("\n**Entry: getTopic** " + "ChannelID: " + channelID);
		List<Topic> topics = null;
		topics = TopicDao.getInstance().getAllTopicsForChannel(channelID);
		return Response.status(200).entity(topics).build();
	}
	
	/**
	 * Gets the top20 topics.
	 *
	 * @return the list of topics
	 * @api {get} /topic/top20 Fetch List of Topics
	 * @apiName GetTopics
	 * @apiGroup Topic
	 * @apiParam {String} id Channel ID.
	 * @apiSuccess {Object[]} topics Topics for the Channel.
	 * @apiError {json} Error-Response: {
	 *       "status": "no_entry_exists"
	 *       "message": ""
	 *     }  
	 * @apiError {json} Error-Response: {
	 *       "status": "error"
	 *       "message": ""
	 *     }
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/top20")
	public Response getTop20Topics() {
		// retrieve the top20 Topics
		logger.info("\n**Entry: getTop20Topics** ");
		List<Topic> topics = null;
		topics = TopicDao.getInstance().getTopNTopics(20);
		return Response.status(200).entity(topics).build();
	}
}