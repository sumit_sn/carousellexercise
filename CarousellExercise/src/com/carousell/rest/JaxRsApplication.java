package com.carousell.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import org.jboss.resteasy.plugins.interceptors.CorsFilter;

/**
 * JAX-RS Application class.
 *
 * @author snarayan
 */
public class JaxRsApplication extends Application {
	private Set singletons;

	public JaxRsApplication() {

	}

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> resources = new HashSet<>();
		
		// add resources
		resources.add(TopicService.class);
        
		//this will register Jackson JSON providers
        resources.add(CustomMapperProvider.class);
		return resources;
	}

	@Override
	public Set<Object> getSingletons() {
		if (singletons == null) {
			CorsFilter corsFilter = new CorsFilter();
			corsFilter.getAllowedOrigins().add("*");
			corsFilter.setAllowCredentials(true);
			corsFilter.setAllowedMethods("GET, POST, PUT, DELETE, OPTIONS, HEAD");
			corsFilter.setAllowedHeaders("X-Requested-With, origin, content-type, accept, authorization");
			corsFilter.setCorsMaxAge(1209600);

			singletons = new HashSet<>();
			singletons.add(corsFilter);
		}
		return singletons;
	}
}
